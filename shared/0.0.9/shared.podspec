Pod::Spec.new do |spec|
    spec.name                     = 'shared'
    spec.version                  = '0.0.9'
    spec.homepage                 = 'Link to a Kotlin/Native module homepage'
    spec.source                   = { 
                                      :http => 'https://gitlab.com/api/v4/projects/58987446/packages/maven/FixedIncome/shared-kmmbridge/0.0.9/shared-kmmbridge-0.0.9.zip',
                                      :type => 'zip',
                                      :headers => ["'Accept: application/octet-stream'"]
                                    }
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'Some description for a Kotlin/Native module'
    spec.vendored_frameworks      = 'shared.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '13'
            
            
end